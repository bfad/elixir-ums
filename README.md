# Ums

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `ums` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:ums, "~> 0.1.0"}]
    end
    ```

  2. Ensure `ums` is started before your application:

    ```elixir
    def application do
      [applications: [:ums]]
    end
    ```

