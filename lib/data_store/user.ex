defmodule Ums.DataStore.User do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query, only: [from: 2]
  alias Ums.User

  schema "users" do
    field :firstName, :string
    field :lastName, :string
    field :username, :string
    field :email, :string
    field :password, :string
    field :password_reset_secret, :string

    timestamps
  end

  @required_fields ~w(firstName username email)a

  def changeset(user, params \\ %{}) do
    user
    |> to_ds_user
    |> cast(params, allowed_fields)
    |> validate_required(@required_fields)
    |> hash_password
  end

  defp allowed_fields do
    __schema__(:fields) |> Enum.reject(&(&1 == :id))
  end

  defp hash_password(changeset) do
    if (password = get_change(changeset, :password)) do
      put_change(changeset, :password, Ums.Password.data_store_hash(password))
    else
      delete_change(changeset, :password)
    end
  end

  def create(user = %User{}, password \\ nil) do
    user
    |> to_ds_user
    |> changeset(%{password: password})
    |> Ums.DataStore.insert!
    |> to_user
  end

  def update(user = %User{}, changes = %{}) do
    user
    |> to_ds_user
    |> changeset(changes)
    |> Ums.DataStore.update!
    |> to_user
  end

  def get(id) do
    Ums.DataStore.get(Ums.DataStore.User, id)
    |> to_user
  end

  def get_by(array) do
    Ums.DataStore.get_by(Ums.DataStore.User, array)
    |> to_user
  end

  defp to_ds_user(user) do
    case user do
      %__MODULE__{} -> user
      %User{} -> struct(__MODULE__, Map.to_list(user))
      nil -> nil
    end
  end

  defp to_user(user) do
    case user do
      %User{} -> user
      %__MODULE__{} -> struct(User, Map.to_list user)
      nil -> nil
    end
  end

  def password?(%User{id: nil}, value) do
    false
  end

  def password?(%User{id: user_id}, value) do
    pw_hash = (from u in "users", where: u.id == ^user_id, select: u.password)
    |> Ums.DataStore.all
    |> hd

    Ums.Password.verify(value, pw_hash)
  end

  def password_reset_secret?(%User{id: user_id}, secret)
  when is_binary(secret) and not is_nil(user_id) do
    (from u in "users", where: u.id == ^user_id, select: u.password_reset_secret)
    |> Ums.DataStore.all
    |> hd() == secret
  end

  def reset_password_for(user = %User{}, password) when is_binary(password) do
    # Converting a user to a ds user sets password_reset_secret to nil which
    # means that setting it to nil with the changeset produces no change. To
    # work around this, I first stash a value in password_reset_secret before
    # creating a changeset.
    user
    |> to_ds_user
    |> Map.put(:password_reset_secret, "value")
    |> Ums.DataStore.User.changeset(%{password: password, password_reset_secret: nil})
    |> Ums.DataStore.update!
  end
end
