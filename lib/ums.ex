defmodule Ums do
  @moduledoc """
  User Management System (Ums)
  ============================
  """

  use Application

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    # Define workers and child supervisors to be supervised
    children = [
      # Starts a worker by calling: Ums.Worker.start_link(arg1, arg2, arg3)
      # worker(Ums.Worker, [arg1, arg2, arg3]),
      worker(Ums.DataStore, []),
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Ums.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
