defmodule Ums.Password do
  @moduledoc """
  Implements methods for hashing, verifying, and formatting passwords
  """

  defp config, do: Application.get_env(:ums, __MODULE__, %{})

  @doc """
  Given a plaintext string it generates a new salt, hashes the string, and
  returns a string formatted for data storage. The formatted string is broken up
  into multiple sections with the "$" delimiter. The current format is:
  "hash_algorithm$#blocks$#interations$salt$hash". Both the salt and the hash
  are unpadded, URL-safe, Base 64 encoded values.
  """
  def data_store_hash(plaintext) do
    blocks = config[:blocks] || 2
    cost   = config[:cost]   || 160_000
    salt   = :crypto.strong_rand_bytes(32)
    hash   = pbkdf2_hash(plaintext, salt, blocks, cost)

    "pbkdf2_sha512$#{blocks}$#{cost}$#{Base.url_encode64 salt, padding: false}$#{Base.url_encode64 hash, padding: false}"
  end

  @doc """
  Takes in plaintext and a string formatted using the data_store_hash/1 function
  and returns true if the formatted string is the derived key for the plaintext
  string provided.
  """
  def verify(plaintext, ds_hash) when is_binary(plaintext) and is_binary(ds_hash) do
    [_, blocks, cost, salt, hash] = String.split(ds_hash, "$")

    pbkdf2_hash(
      plaintext,
      Base.url_decode64!(salt, padding: false),
      String.to_integer(blocks),
      String.to_integer(cost)
    ) == Base.url_decode64!(hash, padding: false)
  end

  @doc """
  Implements the PBKDF2 algorithm with SHA512 to hash a password.
  """
  def pbkdf2_hash(plaintext, salt, blocks \\ 2, cost \\ 96_000) when is_binary(plaintext) do
    pbkdf2_block_iteration(%{
      block:    1,
      blocks:   blocks,
      cost:     cost,
      salt:     salt,
      password: plaintext
    })
  end

  defp pbkdf2_block_iteration(%{block: block, blocks: blocks, password: password, salt: salt, cost: cost})
  when block >= blocks do
    pbkdf2_hash_iteration(cost, password, salt <> <<block::big-integer-size(32)>>)
  end

  defp pbkdf2_block_iteration(%{block: block, blocks: blocks, password: password, salt: salt, cost: cost}) do
    pbkdf2_hash_iteration(cost, password, salt <> <<block::big-integer-size(32)>>) <>
    pbkdf2_block_iteration(%{block: block + 1, blocks: blocks, password: password, salt: salt, cost: cost})
  end

  defp pbkdf2_hash_iteration(1, key, data) do
    hash_function(key, data)
  end

  defp pbkdf2_hash_iteration(cost, key, data) do
    first = hash_function(key, data)
    :crypto.exor(first, pbkdf2_hash_iteration(cost - 1, key, first))
  end

  defp hash_function(key, data) do
    :crypto.hmac(:sha512, key, data)
  end
end
