defmodule Ums.PasswordReset do
  alias Ums.User
  alias Ums.Token
  alias Ums.DataStore

  def new(email) when is_binary(email) do
    user = DataStore.User.get_by(email: email)

    if user do
      {:ok, %Token{type: "password_reset", user: user, issued_at: now, secret: generate_secret}}
    else
      {:error, "Invalid email address"}
    end
  end

  def valid?(token = %Token{}) do
    token.type == "password_reset" &&
      DateTime.to_unix(DateTime.utc_now) - token.issued_at < ttl &&
      DataStore.User.password_reset_secret?(token.user, token.secret)
  end

  def update_password(token = %Token{}, password) when is_binary(password) do
    if valid?(token) do
      DataStore.User.reset_password_for(token.user, password)
      :ok
    else
      {:error, "Invalid token"}
    end
  end

  defp config, do: Application.get_env(:ums, __MODULE__, %{})
  defp ttl, do: config[:ttl] || 60 * 60 * 1

  defp now do
    DateTime.utc_now |> DateTime.to_unix
  end

  defp generate_secret do
    Base.url_encode64(:crypto.strong_rand_bytes(16), padding: false)
  end
end
