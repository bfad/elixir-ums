defmodule Ums.Session do
  alias Ums.User
  alias Ums.Token
  alias Ums.DataStore

  def new(username, password) when is_binary(username) and is_binary(password) do
    user = DataStore.User.get_by(username: username) || %User{}
    if User.password?(user, password) do
      {:ok, %Token{type: "session", user: user, issued_at: now}}
    else
      {:error, "Invalid credentials"}
    end
  end

  def valid?(token = %Token{}) do
    token.type == "session" &&
      DateTime.to_unix(DateTime.utc_now) - token.issued_at < session_ttl
  end

  def extend(token = %Token{type: "session"}) do
    if valid?(token) do
      {:ok, %{token | issued_at: now}}
    else
      {:error, "Invalid Session"}
    end
  end

  defp config, do: Application.get_env(:ums, __MODULE__, %{})
  defp session_ttl, do: config[:session_ttl] || 60 * 60 * 8

  defp now do
    DateTime.utc_now |> DateTime.to_unix
  end
end
