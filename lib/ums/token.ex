defmodule Ums.Token do
  # The secret attribute is meant for JWT's jti claim to prevent replay attacks.
  # Ex. The token generated for resetting a password should be one-time use.
  defstruct user: nil, type: nil, issued_at: nil, secret: nil

  defp config, do: Application.get_env(:ums, __MODULE__, %{})

  def to_jwt(token = %Ums.Token{}) do
    token
    |> to_jwt_claims
    |> JsonWebToken.sign(%{alg: config[:algorithm], key: config[:private_key]})
  end

  def from_jwt(token) when is_binary(token) do
    case JsonWebToken.verify(token, %{alg: config[:algorithm], key: config[:public_key]}) do
      {:ok, claims} ->
        {:ok, from_jwt_claims(claims)}
      {:error, msg} ->
        {:error, msg}
    end
  end

  defp to_jwt_claims(token = %__MODULE__{}) do
    claims = %{
      sub: token.user.id,
      iat: token.issued_at,
      type: token.type
    }
    if token.secret, do: Map.put(claims, :jti, token.secret), else: claims
  end

  defp from_jwt_claims(claims = %{sub: sub, iat: iat, type: type}) do
    %__MODULE__{
      user: Ums.DataStore.User.get(sub),
      type: type,
      issued_at: iat,
      secret: claims[:jti]
    }
  end
end
