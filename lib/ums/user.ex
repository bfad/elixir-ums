defmodule Ums.User do
  defstruct id: nil, firstName: nil, lastName: nil, username: nil, email: nil, created_at: nil, updated_at: nil

  defdelegate password?(user, value), to: Ums.DataStore.User
end
