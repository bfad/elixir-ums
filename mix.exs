defmodule Ums.Mixfile do
  use Mix.Project

  def project do
    [
      app: :ums,
      version: "0.1.0",
      description: "A simple User Management System",
      elixir: "~> 1.3",
      build_embedded: Mix.env == :prod,
      start_permanent: Mix.env == :prod,
      deps: deps,
      package: package,
      preferred_cli_env: [espec: :test],
      spec_pattern: "*.spec.exs",
    ]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    [mod: {Ums, []},
     applications: [:logger, :ecto, :postgrex]]
  end


  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options
  defp deps do
    [
      {:ecto, "~> 2.0"},
      {:postgrex, ">=0.0.0"},
      {:json_web_token, "~> 0.2"},
      {:espec, "~> 1.0.1", only: :test},
    ]
  end

  defp package do
    [
      maintainers: ["Brad Lindsay"],
      licenses: ["Apache 2.0"],
    ]
  end
end
