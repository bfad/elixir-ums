defmodule Ums.DataStore.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :firstName, :string
      add :lastName, :string
      add :username, :string, null: false
      add :password, :text
      add :email, :text, null: false
      add :emailConfirmed, :boolean, default: false

      timestamps null: true
    end
    create index(:users, [:username], unique: true)
    create index(:users, [:email], unique: true)
  end
end
