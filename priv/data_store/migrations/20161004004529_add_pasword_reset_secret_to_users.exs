defmodule Ums.DataStore.Migrations.AddPaswordResetSecretToUsers do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :password_reset_secret, :string
    end
  end
end
