defmodule Ums.DataStore.UserSpec do
  use ESpec
  alias Ums.DataStore.User, as: DSUser
  alias Ums.User
  doctest Ums.DataStore.User
  import Ecto.Query, only: [from: 2]

  @new_user %User{
    firstName: "First",
    lastName:  "Last",
    username:  "username",
    email:     "email@example.com"
  }
  @password "Passw0rdForY@@"

  describe ".create" do
    it "creates a user without a password" do
      user = DSUser.create(@new_user)
      pass = (from u in "users", where: u.id == ^user.id, select: u.password)
      |> Ums.DataStore.all
      |> hd

      expect user |> to(be_struct User)
      expect(user.id).not_to be nil
      expect(pass).to be nil
      expect(user.firstName).to eq @new_user.firstName
      expect(user.lastName).to eq @new_user.lastName
      expect(user.username).to eq @new_user.username
      expect(user.email).to eq @new_user.email
    end

    it "creates a user with the specified password hashed" do
      user = DSUser.create(@new_user, @password)
      pass = (from u in "users", where: u.id == ^user.id, select: u.password)
      |> Ums.DataStore.all
      |> hd

      expect user |> to(be_struct User)
      expect(user.id).not_to be nil
      expect(Ums.Password.verify(@password, pass)).to be true
    end
  end

  describe ".update" do
    let!(:existing, do: DSUser.create(@new_user))
    let(:user) do
      DSUser.create(%User{
        firstName: "Me",
        lastName:  "Myself",
        username:  "used",
        email:     "me@example.com"
      })
    end

    it "throws an error if changing the username to one taken by an existing user" do
      expect(fn -> DSUser.update(user, %{username: existing.username}) end)
      |> to(raise_exception Ecto.ConstraintError)
    end

    it "throws an error if changing the email to one taken by an existing user" do
      expect(fn -> DSUser.update(user, %{email: existing.email}) end)
      |> to(raise_exception Ecto.ConstraintError)
    end

    it "updates a user's fields" do
      changes = %{
        firstName: "I",
        lastName: "Iam",
        email: "i@example.com",
        username: "imcool",
      }
      user = DSUser.update(user, changes)
      pass = (from u in "users", where: u.id == ^user.id, select: u.password)
      |> Ums.DataStore.all
      |> hd

      expect user |> to(be_struct User)
      expect(user.id).not_to be nil
      expect(pass).to be nil
      expect(user.firstName).to eq changes.firstName
      expect(user.lastName).to eq changes.lastName
      expect(user.username).to eq changes.username
      expect(user.email).to eq changes.email
    end

    it "updates a user with the specified password hashed" do
      user = DSUser.update(user, %{password: @password})
      pass = (from u in "users", where: u.id == ^user.id, select: u.password)
      |> Ums.DataStore.all
      |> hd

      expect user |> to(be_struct User)
      expect(Ums.Password.verify(@password, pass)).to be true
    end
  end
end
