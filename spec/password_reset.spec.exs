defmodule Ums.PasswordResetSpec do
  use ESpec
  alias Ums.PasswordReset
  alias Ums.User
  alias Ums.Token

  let :user do
    u = %User{
      firstName:  "First",
      lastName:  "Lastname",
      username: "username",
      email: "foo@example.com"
    }
    Ums.DataStore.User.create(u, password)
  end
  let :password, do: "MyPassword!234"

  describe ".new" do
    it "returns a password-reset token for the user associated with the email address" do
      {:ok, token} = PasswordReset.new(user.email)
      expect(token.type).to eq "password_reset"
      expect(token.user).to eq user
      expect(token.secret).not_to be nil
    end

    it "returns an error if no user with the email address can be found" do
      expect(PasswordReset.new("garbage")).to eq {:error, "Invalid email address"}
    end
  end

  describe ".valid?" do
    let :token, do: %Token{type: type, user: user, issued_at: issued_at, secret: secret}
    let :type, do: "password_reset"
    let :issued_at, do: DateTime.utc_now |> DateTime.to_unix
    let :secret, do: "secret"

    before do
      Ums.DataStore.update! Ums.DataStore.User.changeset(user, %{password_reset_secret: secret})
    end

    it "returns true for a session that isn't expired with a matching secret" do
      expect(PasswordReset.valid?(token)).to be true
    end

    context "The token has expired" do
      let :issued_at, do: 0

      it "returns false" do
        expect(PasswordReset.valid?(token)).to be false
      end
    end

    context "Not a password_reset token" do
      let :type, do: "session"

      it "returns false" do
        expect(PasswordReset.valid?(token)).to be false
      end
    end

    context "The secret doesn't match" do
      let :token, do: %Token{type: type, user: user, issued_at: issued_at, secret: "garbage"}

      it "returns false" do
        expect(PasswordReset.valid?(token)).to be false
      end
    end
  end

  describe ".update_password" do
    let :token, do: %Token{type: type, user: user, issued_at: issued_at, secret: secret}
    let :type, do: "password_reset"
    let :issued_at, do: DateTime.utc_now |> DateTime.to_unix
    let :secret, do: "secret"
    let :new_password, do: "MyB3stNewPa$$word"

    before do
      Ums.DataStore.update! Ums.DataStore.User.changeset(user, %{password_reset_secret: secret})
    end

    it "returns :ok" do
      expect(PasswordReset.update_password(token, new_password)).to eq :ok
    end

    it "updates the password" do
      expect(Ums.DataStore.User.password?(user, new_password)).to be false

      PasswordReset.update_password(token, new_password)

      expect(Ums.DataStore.User.password?(user, new_password)).to be true
    end

    it "clears the secret out of the database" do
      expect(Ums.DataStore.get!(Ums.DataStore.User, user.id).password_reset_secret)
      |> to(eq secret)

      PasswordReset.update_password(token, new_password)

      expect(Ums.DataStore.get!(Ums.DataStore.User, user.id).password_reset_secret)
      |> to(eq nil)
    end

    context "The token has expired" do
      let :issued_at, do: 0

      it "returns an error" do
        expect(PasswordReset.update_password(token, new_password)).to eq {:error, "Invalid token"}
      end
    end

    context "Not a password_reset token" do
      let :type, do: "session"

      it "returns false" do
        expect(PasswordReset.update_password(token, new_password)).to eq {:error, "Invalid token"}
      end
    end

    context "The secret doesn't match" do
      let :token, do: %Token{type: type, user: user, issued_at: issued_at, secret: "garbage"}

      it "returns false" do
        expect(PasswordReset.update_password(token, new_password)).to eq {:error, "Invalid token"}
      end
    end
  end
end
