defmodule Ums.SessionSpec do
  use ESpec
  alias Ums.Session
  alias Ums.User
  alias Ums.Token

  let :user do
    u = %User{
      firstName:  "First",
      lastName:  "Lastname",
      username: "username",
      email: "foo@example.com"
    }
    Ums.DataStore.User.create(u, password)
  end
  let :password, do: "MyPassword!234"

  describe ".new" do
    it "returns a session token for the user associated with those credentials" do
      {:ok, token} = Session.new(user.username, password)
      expect(token.type).to eq "session"
      expect(token.user).to eq user
    end

    it "returns an error if no user with those credentials can be found" do
      expect(Session.new("garbage", "garbage")).to eq {:error, "Invalid credentials"}
    end
  end

  describe ".valid?" do
    let :token, do: %Token{type: type, user: user, issued_at: issued_at}
    let :type, do: "session"
    let :issued_at, do: DateTime.utc_now |> DateTime.to_unix

    it "returns true for a session that isn't expired" do
      expect(Session.valid?(token)).to be true
    end

    context "The session has expired" do
      let :issued_at, do: 0

      it "returns false" do
        expect(Session.valid?(token)).to be false
      end
    end

    context "Not a session token" do
      let :type, do: "password_reset"

      it "returns false" do
        expect(Session.valid?(token)).to be false
      end
    end
  end

  describe ".extend" do
    let :token do
      {:ok, t} = Session.new(user.username, password)
      %{t | issued_at: t.issued_at - offset}
    end
    let :offset, do: 30

    it "returns a token with an updated issued_at" do
      {:ok, new_token} = Session.extend(token)
      expect(new_token.issued_at - token.issued_at).to be :>=, offset
    end

    context "expired session" do
      let :offset, do: 60 * 60 * 24 * 365 * 15

      it "returns an error" do
        expect(Session.extend(token)).to eq {:error, "Invalid Session"}
      end
    end
  end
end
