Mix.Task.run "ecto.create", ~w(-r Ums.DataStore --quiet)
Mix.Task.run "ecto.migrate", ~w(-r Ums.DataStore --quiet)
Ecto.Adapters.SQL.Sandbox.mode(Ums.DataStore, :manual)

ESpec.configure fn(config) ->
  config.before fn(_tags) ->
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Ums.DataStore)
  end

  config.finally fn(_shared) ->
    Ecto.Adapters.SQL.Sandbox.checkin(Ums.DataStore, [])
    :ok
  end
end
