defmodule Ums.TokenSpec do
  use ESpec
  alias Ums.Token
  alias Ums.User

  let :token, do: %Token{user: user, type: "type", issued_at: 42, secret: secret}
  let :secret, do: "secret"
  let :pubkey, do: Application.get_env(:ums, Ums.Token, %{})[:public_key]
  let :user do
    u = %User{
      firstName: "First",
      lastName:  "Lastname",
      username: "username",
      email: "foo@example.com"
    }
    Ums.DataStore.User.create(u, "MyPassword!234")
  end

  describe ".to_jwt" do
    it "translates the elements of the token into registered JWT claims" do
      {:ok, claims} = JsonWebToken.verify(Token.to_jwt(token), %{key: pubkey})

      expect(claims.sub).to eq user.id
      expect(claims.iat).to eq token.issued_at
      expect(claims.type).to eq token.type
      expect(claims.jti).to eq token.secret
    end

    context "no secret" do
      let :secret, do: nil

      it "doesn't add the jti JWT claim" do
        {:ok, claims} = JsonWebToken.verify(Token.to_jwt(token), %{key: pubkey})
        expect(Map.has_key?(claims, :jti)).to be false
      end
    end
  end

  describe ".from_string" do
    it "returns an error if not a valid JWT signature" do
      string = Token.to_jwt(token) <> "abc"
      expect(Token.from_jwt(string)).to eq {:error, "invalid"}
    end
  end

  describe "string serialization" do
    it "should serialize to string and back again" do
      string = Token.to_jwt(token)
      {:ok, back} = Token.from_jwt(string)

      expect(string).to be_binary
      expect(back).to eq token
    end
  end
end
